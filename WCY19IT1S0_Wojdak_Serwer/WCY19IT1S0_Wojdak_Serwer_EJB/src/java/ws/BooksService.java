package ws;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author student
 */
@WebService(serviceName = "BooksService")
@Stateless()
public class BooksService {

    EntityManager em;
    
    public BooksService() {
        
    }
    
    

    @WebMethod(operationName = "getBook")
    public Book getBook(@WebParam(name = "id") Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY19IT1S0_Wojdak_Serwer_PU");
        em = emf.createEntityManager();
        TypedQuery<Book> query = em.createNamedQuery("Book.findById", Book.class).setParameter("id", id);
        return query.getSingleResult();
    }

    
    @WebMethod(operationName = "getAllBooks")
    public List<Book> getAllBooks() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY19IT1S0_Wojdak_Serwer_PU");
        em = emf.createEntityManager();
        TypedQuery<Book> query = em.createNamedQuery("Book.findAll", Book.class);
        return query.getResultList();
    }


    @WebMethod(operationName = "uploadBook")
    public void uploadBook(@WebParam(name = "book") Book book) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY19IT1S0_Wojdak_Serwer_PU");
        em = emf.createEntityManager();
        em.persist(Book.copyOf(book));
    }

    @WebMethod(operationName = "deleteBook")
    public void deleteBook(@WebParam(name = "id") Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY19IT1S0_Wojdak_Serwer_PU");
        em = emf.createEntityManager();
        Book book = getBook(id);
        em.remove(book);
    }
    
}
