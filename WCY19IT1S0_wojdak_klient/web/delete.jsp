<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete book</title>
        <link rel="stylesheet" type="text/css" href="site.css"/>
    </head>
    <body>
        <h1>Really delete?</h1>
        Are you sure you want to delete this book?<br/><br/>
        <a href="DeleteBook?id=${param.id}"><button type="button">Delete</button></a>
        <a href="index.jsp"><button type="button">Back</button></a>
    </body>
</html>
