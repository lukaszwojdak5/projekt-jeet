<%@page import="ws.BooksService"%>
<%@page import="ws.BooksService_Service"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book details</title>
        <link rel="stylesheet" type="text/css" href="site.css"/>
    </head>
    <body>
        <%
            BooksService_Service service = new BooksService_Service();
            BooksService port = service.getBooksServicePort();
            pageContext.setAttribute("book", port.getBook(Integer.valueOf(request.getParameter("id"))));
        %>
        <h1>Book details</h1>
        <div class="box">
            
        <table id="details_table">
            <tr>
                <th>Title</th>
                <td>${pageScope.book.title}</td>
            </tr>
            <tr>
                <th>Author</th>
                <td>${pageScope.book.author}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>${pageScope.book.description}</td>
            </tr>
            <tr>
                <th>Added</th>
                <td>${pageScope.book.added}</td>
            </tr>
        </table>
        <br/><br/>
        
        <a href="index.jsp"><button type="button">Back</button></a>
        <a href="DownloadBook?id=${pageScope.book.id}"><button type="button">Download</button></a>
        <a href="delete.jsp?id=${pageScope.book.id}"><button type="button">Delete</button></a>
        </div>
    </body>
</html>
