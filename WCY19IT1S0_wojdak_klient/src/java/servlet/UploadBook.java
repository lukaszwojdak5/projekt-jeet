/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ws.Book;
import ws.BooksService;
import ws.BooksService_Service;

/**
 *
 * @author student
 */
@MultipartConfig()
public class UploadBook extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //prepare service client
        BooksService soapClient = getBooksServicePort();
        
        //prepare book to send based on form data
        Book newBook = new Book();
        newBook.setTitle(request.getParameter("title"));
        newBook.setAuthor(request.getParameter("author"));
        newBook.setAdded(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date()));
        newBook.setDescription(request.getParameter("description"));
        byte[] content = new byte[(int)request.getPart("file").getSize()];
        request.getPart("file").getInputStream().read(content);
        newBook.setContent(content);
        
        //upload
        soapClient.uploadBook(newBook);
                
        //redirect to success page
        response.sendRedirect("uploaded.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

        private BooksService getBooksServicePort() {
        BooksService_Service service = new BooksService_Service();
        BooksService port = service.getBooksServicePort();
        return port;
    }
}
