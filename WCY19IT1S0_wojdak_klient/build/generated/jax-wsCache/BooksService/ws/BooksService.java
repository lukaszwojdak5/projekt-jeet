
package ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebService(name = "BooksService", targetNamespace = "http://ws/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface BooksService {


    /**
     * 
     * @param id
     * @return
     *     returns ws.Book
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getBook", targetNamespace = "http://ws/", className = "ws.GetBook")
    @ResponseWrapper(localName = "getBookResponse", targetNamespace = "http://ws/", className = "ws.GetBookResponse")
    @Action(input = "http://ws/BooksService/getBookRequest", output = "http://ws/BooksService/getBookResponse")
    public Book getBook(
        @WebParam(name = "id", targetNamespace = "")
        Integer id);

    /**
     * 
     * @return
     *     returns java.util.List<ws.Book>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getAllBooks", targetNamespace = "http://ws/", className = "ws.GetAllBooks")
    @ResponseWrapper(localName = "getAllBooksResponse", targetNamespace = "http://ws/", className = "ws.GetAllBooksResponse")
    @Action(input = "http://ws/BooksService/getAllBooksRequest", output = "http://ws/BooksService/getAllBooksResponse")
    public List<Book> getAllBooks();

    /**
     * 
     * @param book
     */
    @WebMethod
    @RequestWrapper(localName = "uploadBook", targetNamespace = "http://ws/", className = "ws.UploadBook")
    @ResponseWrapper(localName = "uploadBookResponse", targetNamespace = "http://ws/", className = "ws.UploadBookResponse")
    @Action(input = "http://ws/BooksService/uploadBookRequest", output = "http://ws/BooksService/uploadBookResponse")
    public void uploadBook(
        @WebParam(name = "book", targetNamespace = "")
        Book book);

    /**
     * 
     * @param id
     */
    @WebMethod
    @RequestWrapper(localName = "deleteBook", targetNamespace = "http://ws/", className = "ws.DeleteBook")
    @ResponseWrapper(localName = "deleteBookResponse", targetNamespace = "http://ws/", className = "ws.DeleteBookResponse")
    @Action(input = "http://ws/BooksService/deleteBookRequest", output = "http://ws/BooksService/deleteBookResponse")
    public void deleteBook(
        @WebParam(name = "id", targetNamespace = "")
        Integer id);

}
