<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New book</title>
        <link rel="stylesheet" type="text/css" href="site.css"/>
    </head>
    <body>
        <h1>Add new book</h1>
        <form action="UploadBook" method="POST" enctype="multipart/form-data">
            <label for="title">Title</label><br/>
            <input type="text" name="title" id="title" required="true"/><br/><br/>
            <label for="author">Author</label><br/>
            <input type="text" name="author" id="author" required="true"/><br/><br/>
            <label for="description">Description</label><br/>
            <textarea name="description" id="description"></textarea><br/><br/>
            <label for="file">Attach file</label><br/>
            <input type="file" name="file" accept="application/pdf" id="file" required="true"/><br/><br/>
            <a href="index.jsp"><button type="button">Back</button></a>
            <input type="submit" value="Upload"/>
        </form>
    </body>
</html>
