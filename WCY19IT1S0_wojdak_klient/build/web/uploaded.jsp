<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book uploaded</title>
        <link rel="stylesheet" type="text/css" href="site.css"/>
    </head>
    <body>
        <h1>The book has been uploaded!</h1>
        Now you can go back to home page and browse books <br/><br/>
        
        <a href="index.jsp"><button type="button">Back</button></a>
    </body>
</html>
