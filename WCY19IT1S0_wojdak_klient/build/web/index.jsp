<%@page import="java.util.logging.Logger"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="ws.BooksService"%>
<%@page import="ws.BooksService_Service"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Books</title>
        <link rel="stylesheet" type="text/css" href="site.css"/>
    </head>
    <body>
        <%
            BooksService_Service service = new BooksService_Service();
            BooksService port = service.getBooksServicePort();
            pageContext.setAttribute("books", port.getAllBooks());
        %>
        <h1>Welcome to books gallery!</h1>
        <div class="box">
            <a href="upload.jsp"><button type="button">New Book</button></a>
        <br/><br/>
        <table id="gallery_table">
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Added</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="book" items="${pageScope.books}">
                <tr>
                    <td>${book.title}</td>
                    <td>${book.author}</td>
                    <td>${book.added}</td>
                    <td class="actions">
                        <a href="details.jsp?id=${book.id}"><button type="button">Details</button></a>
                        <a href="DownloadBook?id=${book.id}"><button type="button">Download</button></a>
                        <a href="delete.jsp?id=${book.id}"><button type="button">Delete</button></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        </div>
    </body>
</html>
